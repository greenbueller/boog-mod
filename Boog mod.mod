name="Boog mod"
path="mod/boog-mod"
tags={
	"Map"
	"Utilities"
}
supported_version="1.9.*"

replace_path="history/states"
replace_path="map/strategicregions"
replace_path="map/supplyareas"